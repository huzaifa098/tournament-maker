<div class="balk">
    <?php

    if (isset($_SESSION['role']) ){
        if($_SESSION['role']=='admin') {
            $menuItems = array(

                array('home','home'),
                array('toernooien','toernooien'),
                array('scheidsrechterbeheren' , 'scheidsrechterbeheren'),
                array('scheidsrechters' , 'scheidsrechters'),
                array('teamsbeheren' , 'teamsbeheren'),
                array('teams' , 'teams'),


                array('logout','logout')

            );
        } elseif($_SESSION['role']=='scheidsrechter') {
            $menuItems = array(

                array('home','home'),
                array('logout','logout')
            );
        }
    } else {
        $menuItems = array(
            array('login', 'login'),
            array('toernooien','toernooien')

        );
    }
    foreach ($menuItems as $menuItem) {
        echo '<a href="index.php?page='.$menuItem[1].'">'.$menuItem[0].'</a>';
    }
    ?>
</div>