<?php
include 'private/connectie.php';
 $sql = "SELECT * FROM scheidsrechters WHERE active = 1" ;
 $stmt = $conn->prepare($sql);
 $stmt->execute();

 $result = $stmt->fetch();

?>


<form action="php/scheidsrechtersbewerken.php" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputEmail4">voornaam</label>
            <input type="hidden" name="userid" value="<?= $result['userid'] ?>">
            <input type="text" class="form-control"  placeholder="voornaam" name="voornaam" value="<?= $result['voornaam'] ?>">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPassword4">tussenvoegsel</label>
            <input type="text" class="form-control" placeholder="tussenvoegsel" name="tussenvoegsel" value="<?= $result['tussenvoegsel'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputAddress">achternaam</label>
        <input type="text" class="form-control" name="achternaam"  placeholder="achternaam" value="<?= $result['achternaam'] ?>">
    </div>
    <div class="form-group">
        <label for="inputAddress2">email</label>
        <input type="email" class="form-control" placeholder="email" name="email" value="<?= $result['email'] ?>">
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputCity">wachtwoord</label>
            <input type="password" class="form-control" placeholder="wachtwoord" name="wachtwoord" value="<?= $result['wachtwoord'] ?>">
        </div>


        <button type="submit" class="btn btn-success">aanpassen</button>
</form>
