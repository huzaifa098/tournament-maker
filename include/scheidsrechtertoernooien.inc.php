<?php

include 'private/connectie.php';

$sql = "SELECT * FROM toernooi WHERE active = 1";
$stmt = $conn->prepare($sql);
$stmt->execute();

$sql2 = "SELECT * FROM teams";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute();
$sql3 = "SELECT * FROM wedstrijden";
$stmt3 = $conn->prepare($sql3);
$stmt3->execute();


?>
<?php while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"><?= $result['toernooinaam']?></h5>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><?= $result['startdatum']?></li>
        </ul>

        <!-- Modal -->

        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            delete
        </button>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">bevestiging</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        weet je zeker dat je de geselecteerde toernooi wilt verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cencel</button>
                        <form action="php/toernooiverwijderen.php" method="post">
                            <input type="hidden" value="<?= $result['toernooi_id']?>" name="toernooi_id">
                            <button type="submit" class="btn btn-danger">delete</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>

    <form action="index.php?page=scoreinvullen" method="Post">
        <input type="hidden" value="<?= $result['toernooi_id']?>" name="toernooi_id">
        <button type="submit" class="btn btn-success">scoreinvullen</button>
    </form>

<?php } ?>