<?php
include "private/connectie.php";
$toernooi_id = $_POST['toernooi_id'];


$sql = "SELECT t1.logo as t1logo, t1.teamnaam as t1naam, t1.team_id, t2.logo as t2logo, t2.teamnaam as t2naam, t2.team_id FROM wedstrijden w 
        INNER JOIN teams t1 ON w.team1_id = t1.team_id  
        INNER JOIN teams t2 ON w.team2_id = t2.team_id 
        WHERE w.toernooi_id =  :toernooi_id ";
$smt = $conn->prepare($sql);
$smt->execute(array(
    ':toernooi_id' => $toernooi_id
));

while ($result = $smt->fetch(PDO::FETCH_ASSOC)) {

    ?>

    <form enctype="multipart/form-data">
    <div class="matches">
        <div class="match">
            <h1 style="color:white; font-size: 25px; padding-left: 330px"><?= $result['t1naam'] ?></h1>
            <img style="margin-left: 75px;" height="100"  src="data:image/png;base64,<?php echo $result['t2logo'] ?>"/>

        </div>
        <div class="matchdata">
            <div class="score">
                <span>0</span>
            </div>
        </div>
        <div class="matchdata">
            <div class="score">
                <span>0</span>
            </div>
        </div>
        <div class="match">
            <h1 style="color:white; font-size: 25px; padding-left: 330px"><?= $result['t2naam'] ?></h1>
            <img style="margin-left: 75px;" height="100" src="data:image/png;base64,<?php echo $result['t1logo'] ?>"/>
        </div>
    </div>
    </form>

<?php } ?>

<style>
    .matches {
        display: flex;
        background-color: #F0FFFF;
        flex-direction: row;
        justify-content: space-between;
        text-align: center;

    }

    .matchdata {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
    }

    .matchdata .score {
        background-color: white;
        width: 50px;
        height: 50px;



    }
</style>

