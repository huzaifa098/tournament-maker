<?php
include 'private/connectie.php';
 $sql = "SELECT * FROM toernooi WHERE active = 1";
$stmt = $conn->prepare($sql);
$stmt->execute();

$sql = "SELECT * FROM toernooi ";
$stmt = $conn->prepare($sql);
$stmt->execute();

$sql2 = "SELECT * FROM teams";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute();

?>

<h1>toernooi aanmaken</h1>
<form action="php/toernooiaanmaken.php" method="post">

<div class="form-group">
    <label for="exampleFormControlInput1"></label>
    <input type="text" class="form-control"id="exampleFormControlInput1" placeholder="bvb:world cup" name="toernooinaam">
</div>
<div class="form-group">
    <label for="exampleFormControlInput1"></label>
    <input type="date" class="form-control"id="exampleFormControlInput1" placeholder="world cup" name="startdatum">
</div>

    <select name="teams[]" multiple>
        <?php while ($result2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { ?>
            <option value="<?= $result2['team_id'] ?>"><?= $result2['teamnaam']?></option>
        <?php } ?>
    </select>
    <br>
<button type="submit" class="btn btn-success">toernooi toevoegen</button>

</form>
