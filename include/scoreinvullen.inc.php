<?php
include "private/connectie.php";
$toernooi_id = $_POST['toernooi_id'];


$sql = "SELECT wedstrijden_id, t1.logo as t1logo, t1.teamnaam as t1naam, t1.team_id as t1id, t2.logo as t2logo, t2.teamnaam as t2naam, t2.team_id as t2id FROM wedstrijden w 
        INNER JOIN teams t1 ON w.team1_id = t1.team_id  
        INNER JOIN teams t2 ON w.team2_id = t2.team_id 
        WHERE w.toernooi_id =  :toernooi_id AND w.winner IS NULL";
$smt = $conn->prepare($sql);
$smt->execute(array(
    ':toernooi_id' => $toernooi_id
));
$sql3 = "SELECT * FROM wedstrijden";
$stmt3 = $conn->prepare($sql3);
$stmt3->execute();

while ($result = $smt->fetch(PDO::FETCH_ASSOC)) {

    ?>

    <form action="php/scoreinvullen.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="team1_id" value="<?= $result['t1id'] ?>">
        <input type="hidden" name="team2_id" value="<?= $result['t2id'] ?>">
        <input type="hidden" name="toernooi_id" value="<?= $_POST['toernooi_id'] ?>">
        <input type="hidden" value="<?= $_SESSION['melding'] = "hallo!"; ?>">

        <div class="matches">
            <div class="match">
                <h1 style="color:white; font-size: 25px; padding-left: 330px"><?= $result['t1naam'] ?></h1>
                <img style="margin-left: 75px;" height="100"
                     src="data:image/png;base64,<?php echo $result['t2logo'] ?>"/>


            </div>
            <div class="matchdata">
                <div class="score">
                    <span><input type="text" placeholder="0" name="uitslag1"></span>


                </div>
            </div>
            <div class="matchdata">
                <div class="score">
                    <span><input type="text" placeholder="0" name="uitslag2"></span>
                </div>
            </div>
            <div class="match">
                <h1 style="color:white; font-size: 25px; padding-left: 330px"><?= $result['t2naam'] ?></h1>
                <img style="margin-left: 75px;" height="100"
                     src="data:image/png;base64,<?php echo $result['t1logo'] ?>"/>
            </div>
        </div>
        <input type="hidden" value="<?= $result['wedstrijden_id'] ?>" name="wedstrijden_id">
        <button type="submit" name="submit" class="btn btn-success">opslaan</button>
    </form>


<?php } ?>

<form action="php/scoreinvullen.php" method="post">
    <input type="hidden" value="newround" name="submit">
    <input type="hidden" name="toernooi_id" value="<?= $_POST['toernooi_id'] ?>">
    <button type="submit" >nieuwe ronden</button>
</form>

<style>
    .matches {
        display: flex;
        background-color: #F0FFFF;
        flex-direction: row;
        justify-content: space-between;
        text-align: center;

    }

    .matchdata {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
    }

    .matchdata .score {
        background-color: white;
        width: 50px;
        height: 50px;


    }
</style>



