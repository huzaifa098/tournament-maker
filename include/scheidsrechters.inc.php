<?php
include 'private/connectie.php';
$sql = "SELECT * FROM scheidsrechters WHERE active= 1";
$stmt = $conn->prepare($sql);
$stmt->execute();

?>
<h1 >scheidsrechters</h1>
<div class="teams">
<?php while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){  ?>

    <div class="card" style="width: 18rem;">


    <div class="card-body">
        <h5 class="card-title"><?= $result['voornaam']?></h5>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item"><?= $result['email']?></li>
        <li class="list-group-item"><?= $result['wachtwoord']?></li>
    </ul>
    <div class="card-body">
      <!----<form action="php/scheidsrechtersverwijderen.php" method="post">-->

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            delete
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">bevestiging</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        weet je zeker dat je de geselecteerde scheidsrechter wilt verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cencel</button>
                       <form action="php/scheidsrechtersverwijderen.php" method="post">
                        <input type="hidden" value="<?= $result['userid']?>" name="userid">
                        <button type="submit" class="btn btn-danger">delete</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>

       <!-- </form>-->
        <form action="index.php?page=scheidsrechtersbewerken" method="post">
            <button type="submit" class="btn btn-primary">
                edit
            </button>
        </form>
    </div>
</div>



<?php } ?>
</div>