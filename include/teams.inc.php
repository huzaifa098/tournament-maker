<?php
include "private/connectie.php";

$sql = "SELECT * FROM teams WHERE active = 1";
$stmt = $conn->prepare($sql);
$stmt->execute();
?>

<h1>teams</h1>
<div class="teams">
    <?php while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>

        <div class="card" style="width: 18rem;">
            <div class="card-body">

            </div>
            <ul class="list-group list-group-flush">
                <img src="data:image/png;base64,<?php echo $result['logo'] ?>"/>
                <li class="list-group-item"><?= $result['teamnaam'] ?></li>
                <li class="list-group-item"><?= $result['afkorting'] ?></li>

            </ul>
            <div class="card-body">
                <div class="card-body">
                    <!----<form action="php/scheidsrechtersverwijderen.php" method="post">-->

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        delete
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">bevestiging</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    weet je zeker dat je de geselecteerde team wilt verwijderen?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cencel
                                    </button>
                                    <form action="php/teamsverwijderen.php" method="post">
                                        <input type="hidden" value="<?= $result['team_id'] ?>" name="team_id">
                                        <button type="submit" class="btn btn-danger">delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    </form>
                    <form action="index.php?page=teamsbewerken" method="post">
                        <button type="submit" class="btn btn-primary">
                            edit
                        </button>

                    </form>
                </div>
            </div>
        </div>


    <?php } ?>
</div>