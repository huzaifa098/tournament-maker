<?php
include "private/connectie.php";
$sql = "SELECT * FROM teams";
$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->fetch()
?>

<form action="php/teamsbewerken.php" method="post" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-md-6">
            <input type="hidden" name="team_id" value="<?=$result['team_id']?>">
            <label for="inputEmail4">teamnaam</label>


               <input type="text" class="form-control"  placeholder="teamnaam" name="teamnaam">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPassword4">afkorting</label>
            <input type="text" class="form-control" placeholder="afkorting(3)" name="afkorting">
        </div>
    </div>
    <div class="form-group">
        <label for="inputAddress">logo</label>
        <input type="file" class="form-control" name="logo">
    </div>
    <button type="submit" class="btn btn-success">aanpassen</button>
</form>
