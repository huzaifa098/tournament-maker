<?php
include '../private/connectie.php';
session_start();
if ($_SESSION['role'] == 'scheidsrechter') {
    if (isset($_SESSION['melding'])) {
        echo $_SESSION['melding'];
        unset ($_SESSION['melding']);
    }
} elseif ($_SESSION['role'] == 'Admin') {
    echo "hallo";
}
$toornooid = $_POST['toernooi_id'];


if ($_POST['submit']) {
    echo "hallo! we genereren nu een nieuwe ronden";
    $sql = " SELECT MIN(ronde) AS 'MIN' FROM `wedstrijden` WHERE toernooi_id = :toernooi_id AND winner IS NOT NULL";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':toernooi_id' => $toornooid
    ));

    $g = $stmt->fetchColumn();


    $stmt = $conn->prepare("SELECT winner FROM wedstrijden WHERE toernooi_id = :tournooi_id AND winner IS NOT NULL AND  ronde = :ronde");
    $stmt->execute([
        ':tournooi_id' => $toornooid,
        ':ronde' => $g
    ]);
    $r = $stmt->fetchAll(PDO::FETCH_ASSOC);

   if ($g == 1) {


        $stmt = $conn->prepare('UPDATE toernooi SET einddatum = :date  WHERE toernooi_id = :toernooi_id');
        $stmt->execute([
            ':date' => date("Y-m-d H:i:s"),
            // ':winnaar' => $r['winner'],
            ':toernooi_id' => $toornooid
        ]);
        echo '<br> dit is de finale <br>';
        die();
    }
    $nieuweronde = $g / 2;


    for ($i = 0; $i < $g; $i += 2) {
        $teamA = $r[$i];
        $teamB = $r[$i + 1];
        $sql2 = "INSERT INTO wedstrijden(team1_id,team2_id,toernooi_id)
      VALUES(:team1_id,:team2_id,:toernooi_id )";
        $sql3 = "UPDATE wedstrijden SET team1_id = :team1_id , team2_id = :team2_id  WHERE toernooi_id = :toernooi_id AND ronde = $nieuweronde";
        $stmt2 = $conn->prepare($sql3);
        $stmt2->execute(array(
            ":team1_id" => $teamA['winner'],
            ":team2_id" => $teamB['winner'],
            ":toernooi_id" => $toornooid,

        ));

    }


} else {



    $team1_id = $_POST['team1_id'];
    $team2_id = $_POST['team2_id'];
    $uitslag1 = $_POST['uitslag1'];
    $uitslag2 = $_POST['uitslag2'];
    $wedstrijden_id = $_POST['wedstrijden_id'];

    if ($uitslag1 == $uitslag2) {
        echo "gelijkspel";
    } else {
        if ($uitslag1 > $uitslag2) {
            echo "team 1 heeft gewonnen";
            $sql = "UPDATE wedstrijden SET uitslag1 = :uitslag_1, uitslag2 = :uitslag_2, winner = :team1_id, loser = :team2_id WHERE wedstrijden_id = :wedstrijden_id";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array(
                ':uitslag_1' => $uitslag1,
                ':uitslag_2' => $uitslag2,
                ':team1_id' => $team1_id,
                ':team2_id' => $team2_id,
                ':wedstrijden_id' => $wedstrijden_id,
            ));
        }
        if ($uitslag2 > $uitslag1) {
            $sql = "UPDATE wedstrijden SET uitslag1 = :uitslag_1, uitslag2 = :uitslag_2, winner = :team2_id, loser = :team1_id  WHERE wedstrijden_id = :wedstrijden_id";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array(
                ':uitslag_1' => $uitslag1,
                ':uitslag_2' => $uitslag2,
                ':team1_id' => $team1_id,
                ':team2_id' => $team2_id,
                ':wedstrijden_id' => $wedstrijden_id,
            ));
        }
    }
}


header('location: ../index.php?page=scheidsrechtertoernooien');