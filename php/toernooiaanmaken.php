<?php
include "../private/connectie.php";
session_start();
//echo "<pre>", print_r($_POST), "</pre>";

// Getting variables
$ronde = count($_POST["teams"]) / 2;
$gekozenteams = shuffle($_POST["teams"]);
$toernooinaam = $_POST['toernooinaam'];
$startdatum = $_POST['startdatum'];
$gekozenteams = $_POST['teams'];
$userid = $_SESSION['userid'];
//var_dump($_POST['teams']);
// Inserting the Tournament
$sql = "INSERT INTO toernooi(toernooinaam, startdatum ) VALUES (:toernooinaam ,:startdatum )";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':toernooinaam' => $toernooinaam,
    ':startdatum' => $startdatum,
));
$toernooi_id = $conn->lastInsertId();

// Connecting the Teams to the Tournament
foreach ($gekozenteams as $gekozenteam) {
    $sql1 = "INSERT INTO team_toernooi( team_id ,toernooi_id) VALUES (:team_id , :toernooi_id)";
    $res1 = $conn->prepare($sql1);
    $res1->execute(array(
        ':team_id' => $gekozenteam,
        ':toernooi_id' => $toernooi_id
    ));
}
// Inserting first round
for ($i = 0; $i < ($ronde * 2); $i += 2) {
    $sql2 = "INSERT INTO wedstrijden(team1_id,team2_id,toernooi_id,ronde)
             VALUES(:team1_id,:team2_id,:toernooi_id,:ronde )";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute(array(
        ":team1_id" => $gekozenteams[$i],
        ":team2_id" => $gekozenteams[$i + 1],
        ":toernooi_id" => $toernooi_id,
        ":ronde" => $ronde,
    ));
}
do {
    // query for selecting round winners
    $sql3 = "SELECT wedstrijden_id
             FROM wedstrijden
             WHERE ronde = :ronde
             AND toernooi_id = :toernooi_id";
    $stmt3 = $conn->prepare($sql3);
    $stmt3->execute(array(
        ':toernooi_id' => $toernooi_id,
        ':ronde' => $ronde,
    ));
    $r = $stmt3->fetchAll(PDO::FETCH_ASSOC);
    //var_dump($ronde);

    //var_dump($r);

    $ronde = $ronde / 2;
    for ($i = 0; $i < ($ronde * 2); $i = $i + 2) {
        $sql4 = "INSERT INTO wedstrijden (toernooi_id, ronde)
                         VALUES (:toernooi_id, :ronde)";
        $stmt4 = $conn->prepare($sql4);
        $stmt4->execute(array(
            ':toernooi_id' => $toernooi_id,
            ':ronde' => $ronde,
           // ':winnaar1' => $r[$i]['wedstrijden_id'],
            //':winnaar2' => $r[$i + 1]['wedstrijden_id'],
        ));
    }


} while ($ronde > 1);




header('location: ../index.php?page=toernooien');



