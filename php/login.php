<?php
include "../private/connectie.php";
session_start();
$sgl = 'SELECT wachtwoord, role,id FROM login WHERE email = :username';
$sth = $conn->prepare($sgl);
$sth->bindParam(':username', $_POST['email']);
$sth->execute();
if ($rsuser = $sth->fetch(PDO::FETCH_ASSOC)) {

    if ($_POST['wachtwoord'] == $rsuser['wachtwoord']) {
        $_SESSION['role'] = $rsuser['role'];
        $_SESSION['userid'] = $rsuser['id'];
        if ($rsuser['role'] == "scheidsrechter") {
            header('location:../index.php?page=scheidsrechtertoernooien');

        } elseif ($rsuser['role'] == "admin") {
            echo "<pre>", print_r($_SESSION), "</pre>";
            header('location:../index.php?page=home');
        }
        else {

            $_SESSION['melding'] = 'gebruikersnaam of wachtwoord incorrect';
            header('location:../index.php?page=login');
        }
    }
}
?>