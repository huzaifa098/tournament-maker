<?php
include'../private/connectie.php';

$userid = $_POST['userid'];
$voornaam = $_POST['voornaam'];
$tussemvoegsel = $_POST['tussenvoegsel'];
$achternaam = $_POST['achternaam'];
$email = $_POST['email'];
$wachtwoord = $_POST['wachtwoord'];

$sql = "UPDATE scheidsrechters SET voornaam = :voornaam , tussenvoegsel = :tussenvoegsel 
, achternaam = :achternaam , email = :email , wachtwoord = :wachtwoord WHERE userid = :userid";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':userid' => $userid,
    ':voornaam' => $voornaam,
    ':tussenvoegsel' => $tussemvoegsel,
    ':achternaam' => $achternaam,
    ':email' => $email,
    ':wachtwoord' => $wachtwoord

));
header('location: ../index.php?page=scheidsrechters');
