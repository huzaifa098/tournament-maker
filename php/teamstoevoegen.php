<?php
include "../private/connectie.php";


$teamnaam = $_POST['teamnaam'];
$afkorting = $_POST['afkorting'];
$logo = base64_encode(file_get_contents($_FILES['logo']['tmp_name']));
try {
    $sql = "INSERT INTO teams(teamnaam , afkorting , logo) VALUES (:teamnaam , :afkorting , :logo)";

    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':teamnaam' => $teamnaam,
        ':afkorting' => $afkorting,
        ':logo' => $logo

    ));


    header("location: ../index.php?page=teams");
}
catch (Exception $e){
    echo '<script>
        alert("ben je dom deze team bestaat er al");
        window.location.href="../index.php?page=teams";
        </script>';
}