<?php
include "../private/connectie.php";

$team_id = $_POST['team_id'];
$teamnaam = $_POST['teamnaam'];
$afkorting = $_POST['afkorting'];
$logo = base64_encode(file_get_contents($_FILES['logo']['tmp_name']));

$sql = "UPDATE teams SET teamnaam = :teamnaam , afkorting = :afkorting , logo = :logo WHERE team_id = :team_id";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
   ':team_id' => $team_id,
   ':teamnaam' => $teamnaam,
   ':afkorting' => $afkorting,
   ':logo' => $logo
));

header('location: ../index.php?page=teams');