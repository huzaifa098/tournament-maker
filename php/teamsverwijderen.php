<?php
include "../private/connectie.php";

$team_id = $_POST['team_id'];

$sql = "UPDATE teams SET active = 0 WHERE team_id = :team_id";
$res = $conn->prepare($sql);
$res->bindParam(':team_id', $team_id);
$res->execute();

header("location: ../index.php?page=teams");