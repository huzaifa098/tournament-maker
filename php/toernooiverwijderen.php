<?php
include '../private/connectie.php';
$toernooi_id = $_POST['toernooi_id'];

$sql = "UPDATE toernooi SET active = 0 WHERE toernooi_id = :toernooi_id";
$res = $conn->prepare($sql);
$res->bindParam(':userid', $toernooi_id);
$res->execute(array(
    ':toernooi_id' => $toernooi_id
));
header('location: ../index.php?page=toernooien');
